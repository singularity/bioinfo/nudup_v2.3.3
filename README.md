#  Package nudup version 2.3.3
https://github.com/tecangenomics/nudup

Marks/removes PCR introduced duplicate molecules based on the molecular tagging
technology used in Tecan products.

Package installation using Miniconda2 V4.7.10 All packages are in /opt/miniconda/bin & are in PATH nudup Version: 2.3.3
Singularity container based on the recipe: Singularity.nudup_v2.3.3.def
## Local build:
```bash
sudo singularity build nudup_v2.3.3.sif Singularity.nudup_v2.3.3.def
```
## Get image help:

```bash
singularity run-help nudup_v2.3.3.sif
```
### Default runscript: nudup
## Usage:
```bash
./nudup_v2.3.3.sif --help
```
or:
```bash
singularity exec nudup_v2.3.3.sif nudup --help
```
image singularity (V>=3.3) is automacly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml
can be pull (singularity version >=3.3) with:

```bash
singularity pull nudup_v2.3.3.sif oras://registry.forgemia.inra.fr/singularity/bioinfo/nudup_v2.3.3/nudup_v2.3.3:latest
```